

#include "VirtualWire.h"


const int potpin0 = A0;  // analog pin used to connect the potentiometer
const int potpin1 = A1;  // analog pin used to connect the potentiometer
const int potpin2 = A2;  // analog pin used to connect the potentiometer

const int led =13;

void setup() {
  Serial.begin(9600);
  // Initialise the IO and ISR
  vw_set_ptt_inverted(true); // Required for DR3100
  vw_setup(2000);  // Bits per sec
  pinMode(led, OUTPUT);
}

void loop()
{
byte msg[3];// create a 3 byte data array
// map the values to byte range for the servo.
  msg[0] = (byte)map(analogRead(potpin0), 0, 1023, 27, 160);     // scale it to use it with the servo (value between 0 and 180)
  msg[1] = (byte)map(analogRead(potpin1), 0, 1023, 27, 160);     // scale it to use it with the servo (value between 0 and 180)
  msg[2] = (byte)map(analogRead(potpin2), 0, 1023, 27, 160);     // scale it to use it with the servo (value between 0 and 180)


  if (true)                            
  {
    Serial.print(msg[0]);
    Serial.println();
    Serial.print(msg[1]);
    Serial.print(" ");
    Serial.print(msg[2]);
    Serial.println();
  }


    digitalWrite(13, true); // Flash a light to show transmitting
    vw_send(msg, 3);
    vw_wait_tx(); // Wait until the whole message is gone
    digitalWrite(13, false);
//    delay(20);

  
  delay(30);   // waits for the servo to get there
}

