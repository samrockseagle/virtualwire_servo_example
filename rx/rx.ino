#include "ServoTimer2.h"  // the servo library
#include "VirtualWire.h"

// define the pins for the servos
#define rollPin  2
#define pitchPin 3
#define yawPin   4

ServoTimer2 servoRoll;    // declare variables for up to eight servos
ServoTimer2 servoPitch;
ServoTimer2 servoYaw;

const int led = 13;

uint8_t buf[VW_MAX_MESSAGE_LEN];//3 is expected number. Max allowed per packet VW_MAX_MESSAGE_LEN
uint8_t buflen = VW_MAX_MESSAGE_LEN;

void debug_print() {

  Serial.print("Got: ");

  for (byte i = 0; i < buflen; i++)
  {
    Serial.print(buf[i], HEX);
    Serial.print(" ");
  }
  Serial.println("");
}

void setup() {
  Serial.begin(9600);

  vw_set_ptt_inverted(true); // Required for DR3100
  vw_setup(2000);   // Bits per sec
  vw_rx_start();       // Start the receiver PLL running

  servoRoll.attach(rollPin);     // attach a pin to the servos and they will start pulsing
  servoPitch.attach(pitchPin);
  servoYaw.attach(yawPin);

  pinMode(led, OUTPUT);
}

void loop() {

  if (vw_get_message(buf, &buflen)) // Non-blocking.. The buflen will be modified to 3 on receiving a new data packet.
  {
    digitalWrite(13, true); // Flash a light to show received good message
    debug_print();
    digitalWrite(13, false);
    if (buflen == 3) {
      servoRoll.write(buf[0]);                         // sets the servo position according to the scaled value
      servoPitch.write(buf[1]);                         // sets the servo position according to the scaled value
      servoYaw.write(buf[2]);
    }
  }

}
